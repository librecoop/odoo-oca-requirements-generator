# Odoo OCA requirements generatos

Small tool to generate a *requirements.txt* file based on a *manifest.json* file extracted from an odoo backup.

dependency: `requests`

#!/bin/python3

import json
import requests
import os
import argparse
from get_valid_modules import get_covered

parser = argparse.ArgumentParser(
     description='Generate requirements.txt by reading a manifest.json file from an odoo backup'
)
parser.add_argument('manifest_file', type=argparse.FileType('r'))
parser.add_argument('requirements_file', type=argparse.FileType('w+'))
parser.add_argument('--odoo_version', type=int, default=13)
parser.add_argument('--extra_addons', action='append', help='Path to addons that will be installed manually, each addon must have its own subfolder')
parser.add_argument('--no_update', action='store_true', help='Update pip module to latest version available. Only affects if odoo_version is the same as the one from the manifest file')
parser.add_argument('--no_dev', action='store_true', help='If the PyPI module only has dev version available, skip it')
parser.add_argument('--always_latest', action='store_true', help='Always update to the most recent version, even if its dev')
args = parser.parse_args()

manifest = json.load(args.manifest_file)

# Use github API to get the list of internal modules in the addons directory
repo_json = requests.get(f'https://api.github.com/repos/odoo/odoo/git/trees/{args.odoo_version}.0').json()
addons_url = [t['url'] for t in repo_json['tree'] if t['path'] == 'addons'][0]
internal_modules = [t['path'] for t in requests.get(addons_url).json()['tree']] + ['base']

covered, deleted = get_covered(manifest['version_info'][0], args.odoo_version)

for module_name, manifest_version in manifest['modules'].items():
    if module_name in internal_modules:
        print(f'{module_name} es interno de odoo')
        continue

    elif args.extra_addons:
        found = False
        for dir in args.extra_addons:
            if os.path.exists(f'{dir}/{module_name}/'):
                print(f'{module_name} encontrado en extra_addons {dir}')
                found = True
                break
        if found:
            continue

    elif module_name in deleted:
        print(f'{module_name} ya no existe en esta version')
        continue

    pypi_req = requests.get(f'https://pypi.org/pypi/odoo{args.odoo_version if args.odoo_version < 15 else ''}-addon-{module_name}/json')

    if not pypi_req.ok:
        print(f'{module_name} no encontrado 🛑')
        continue

    pypi_json = pypi_req.json()

    dev_version, version = None, None
    for v, d in sorted(pypi_json['releases'].items(), key=lambda r: r[1][0]['upload_time'], reverse=True):
        if v.split('.')[0] == str(args.odoo_version):
            if 'dev' not in v:
                version = v
                break
            elif not dev_version:
                dev_version = v

    if args.always_latest and dev_version:
        # if there is no dev_version, it means that the normal version already is the latest
        version = dev_version

    if dev_version and not version:
        print(f'{module_name} only has dev version available {dev_version} ⚠️', end='')
        if args.no_dev:
            print(' skipping because of --no_dev')
            continue
        else:
            print()
            version = dev_version

    if not version:
        print(f'no encontrado version {args.odoo_version} de {module_name} 🛑')
        continue

    args.requirements_file.write(f'{pypi_json['info']['name']}==')

    if args.no_update and str(args.odoo_version) == manifest_version.split('.')[0]:
        args.requirements_file.write(manifest_version)

    else:
        args.requirements_file.write(version)

        if version != manifest_version:
            args.requirements_file.write(f' # Original version {manifest_version}')

    args.requirements_file.write('\n')

    print(f'added {module_name} version {version}')


#!/bin/python3

import os, requests, re, csv
import argparse

def get_covered(start, end, installed=None):
    covered = set()
    deleted = set()
    for version in range(start+1, end+1):
        url = f'https://github.com/OCA/OpenUpgrade/raw/{version}.0/docsource/modules{version-1}0-{version}0.rst'
        doc = requests.get(url)

        is_covered = re.compile(r'\|\s*(\|\w+\||)\s*([_a-z0-9]+)\s*(\|\s*)*(\w*)')

        covered = set()
        for line in doc.text.split('\n'):
            match = is_covered.match(line)
            if match:
                name = match.group(2)

                if match.group(4):
                    covered.add(name)

                if match.group(1) == '|del|':
                    deleted.add(name)

        if installed:
            not_covered = installed - covered - deleted
            # print(len(installed), len(covered), len(not_covered))
            print(f'{version}: {sorted(not_covered)}')

    return covered, deleted


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file', type=argparse.FileType('r'))
    parser.add_argument('start', type=int)
    parser.add_argument('end', type=int)
    args = parser.parse_args()

    installed = set()
    for row in csv.reader(args.file):
        if row[2] == 'Odoo S.A.':
            installed.add(row[1])

    covered, deleted = get_covered(args.start, args.end, installed)
